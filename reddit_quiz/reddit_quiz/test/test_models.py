"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.test import TestCase

from reddit_quiz.models import Quiz, Question, AnswerOption, BaseCode


class QuizTest(TestCase):

    def test_simple_quiz(self):
        quiz = Quiz.objects.create(title="Simple Quiz")
        question1 = Question.objects.create(question="What's 1+1", quiz=quiz)
        AnswerOption.objects.create(answer="0", question=question1)
        AnswerOption.objects.create(answer="1", question=question1)
        AnswerOption.objects.create(answer="2", question=question1, is_correct_answer=True)
        AnswerOption.objects.create(answer="3", question=question1)

        quiz.refresh_from_db()
        question1.refresh_from_db()
        self.assertEqual(quiz.questions.count(), 1)
        self.assertEqual(question1.answer_options.count(), 4)

    def test_unique_code__creation(self):
        code = Quiz.objects.create(title="Test quiz").code
        self.assertIsNotNone(code)
        self.assertEqual(len(code), BaseCode.code.field.max_length)

    def test_unique_code__resave(self):
        """
        After saving, the code shouldn't change
        """
        quiz = Quiz.objects.create(title="Test quiz")
        code = quiz.code
        quiz.title = "New quiz title"
        quiz.save()
        quiz.refresh_from_db()
        self.assertEqual(quiz.code, code)
