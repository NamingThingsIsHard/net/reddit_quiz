"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import factory
from factory.django import DjangoModelFactory

from reddit_quiz import models


class TokenFactory(DjangoModelFactory):
    class Meta:
        model = models.Token

    user = factory.Sequence(lambda n: 'user%d' % n)


class QuizFactory(DjangoModelFactory):
    class Meta:
        model = models.Quiz

    title = factory.Sequence(lambda n: 'quiz%d' % n)


class QuestionFactory(DjangoModelFactory):
    question = factory.Sequence(lambda n: 'question%d' % n)
    quiz = factory.SubFactory(QuizFactory)


class AnswerOptionFactory(DjangoModelFactory):
    class Meta:
        model = models.AnswerOption

    answer = factory.Sequence(lambda n: 'answer%d' % n)
    question = factory.SubFactory(QuestionFactory)
