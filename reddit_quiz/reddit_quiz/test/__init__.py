"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from reddit_quiz.models import Quiz, Token
from reddit_quiz.serializers.create import QuizSerializer


class QuizFixturesMixin:
    @property
    def simple_quiz_dict(self):
        return {
            "title": "Standard quiz",
            "questions": [
                {
                    "question": "What's 1 + 1",
                    "answer_options": [
                        {"answer": "0", },
                        {"answer": "1", "is_correct_answer": True},
                        {"answer": "2", },
                        {"answer": "3", },
                    ]
                },
                {
                    "question": "Who was the King?",
                    "answer_options": [
                        {"answer": "Antonio", },
                        {"answer": "Reganar", },
                        {"answer": "Killian", },
                        {"answer": "Valerio", "is_correct_answer": True},
                    ]
                },
            ]
        }

    @property
    def simple_quiz(self) -> Quiz:
        serializer = QuizSerializer(data=self.simple_quiz_dict)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.instance

    @property
    def token(self):
        return Token.objects.create(user="another_random_user")
