"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from reddit_quiz.serializers.create import QuizSerializer
from reddit_quiz.test import QuizFixturesMixin


class TestQuizSerializer(QuizFixturesMixin, TestCase):

    def test_create(self):
        serializer = QuizSerializer(data=self.simple_quiz_dict)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        quiz = serializer.instance
        self.assertEqual(quiz.questions.count(), 2)
        for question in quiz.questions.all():
            self.assertEqual(question.answer_options.count(), 4)

    def _test_bad_answer_count(self, expected_regex: str, *answer_options: dict):
        """
        Test cases where the number of correct answers != 1
        """
        with self.assertRaisesRegex(ValidationError, expected_regex=expected_regex):
            serializer = QuizSerializer(data={
                "title": "Something's wrong here",
                "questions": [{
                    "question": "What's wrong?",
                    "answer_options": list(answer_options)
                }]
            })
            serializer.is_valid(raise_exception=True)

    def test_no_answers(self):
        self._test_bad_answer_count("at least one correct answer")

    def test_no_correct_answers(self):
        self._test_bad_answer_count(
            "at least one correct answer",
            {"answer": "nothing"},
            {"answer": "Absolutely nothing"},
            {"answer": "Already told you"},
        )

    def test_too_many_correct_answers(self):
        self._test_bad_answer_count(
            "one correct answer",
            {"answer": "nothing", "is_correct_answer": True},
            {"answer": "Absolutely nothing", "is_correct_answer": True},
            {"answer": "Already told you", "is_correct_answer": True},
        )
