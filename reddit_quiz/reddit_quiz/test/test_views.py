"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from reddit_quiz.models import Quiz, Token, Evaluation
from reddit_quiz.test import QuizFixturesMixin

JSON_CONTENT = "application/json"


class TestCreateQuiz(QuizFixturesMixin, TestCase):

    def test_create(self):
        token = self.token
        response = self.client.post(
            reverse("quiz_create"),
            content_type=JSON_CONTENT,
            data=self.simple_quiz_dict,
            QUERY_STRING=f"token={token.code}",
        )
        self.assertEqual(response.status_code, 201, msg=response.content)

        quiz = Quiz.objects.first()
        self.assertIsNotNone(quiz)

        self.assertFalse(Token.objects.filter(code=token.code).exists())


class TestEvaluateQuiz(QuizFixturesMixin, TestCase):

    def _build_answers(self, quiz, should_be_correct=True):
        # TODO: add this to utils
        answers = []
        for question in quiz.questions.all():
            answers.append(
                {
                    "question": question.code,
                    "answer": question.answer_options.filter(
                        is_correct_answer=should_be_correct or None).first().code
                }
            )
        return answers

    def test_correct_answers(self):
        quiz = self.simple_quiz
        token = self.token
        self._eval_quiz(quiz, token)

        # The token should be removed after usage
        self.assertFalse(Token.objects.filter(code=token.code).exists())

        # Make sure we stored the correct evaluation
        evaluations = Evaluation.objects.filter(username=token.user)
        self.assertEqual(evaluations.count(), 1)
        evaluation = evaluations.first()
        self.assertEqual(evaluation.attempts, 1)
        self.assertTrue(evaluation.solved)


    def test_incorrect_answers(self):
        quiz = self.simple_quiz
        token = self.token
        self._eval_quiz(quiz, token, 406, False)

        # Make sure we stored the evaluation
        evaluations = Evaluation.objects.filter(username=token.user)
        self.assertEqual(evaluations.count(), 1)
        evaluation = evaluations.first()
        self.assertEqual(evaluation.attempts, 1)
        self.assertFalse(evaluation.solved)

        # The token should still exist to make further attempts
        self.assertTrue(Token.objects.filter(code=token.code).exists())

    def test_max_incorrect_answers(self):
        quiz = self.simple_quiz
        token = self.token
        for attempt in range(settings.RQ_MAX_ATTEMPTS-1):
            self._eval_quiz(quiz, token, 406, False)

        self._eval_quiz(quiz, token, 406, False)

        # Make sure we stored the evaluation
        evaluations = Evaluation.objects.filter(username=token.user)
        self.assertEqual(evaluations.count(), 1)
        evaluation = evaluations.first()
        self.assertEqual(evaluation.attempts, settings.RQ_MAX_ATTEMPTS)
        self.assertFalse(evaluation.solved)

        # The token should not exist anymore after too many attempts
        self.assertFalse(Token.objects.filter(code=token.code).exists())

    def _eval_quiz(self, quiz, token, expected_status_code=200, should_be_correct=True):
        response = self.client.post(
            reverse("quiz_evaluate", args=[quiz.code]),
            content_type=JSON_CONTENT,
            data=self._build_answers(quiz, should_be_correct=should_be_correct),
            QUERY_STRING=f"token={token.code}",
        )
        self.assertEqual(response.status_code, expected_status_code, msg=response.content)
