"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models

from reddit_quiz import utils


class BaseCode(models.Model):
    class Meta:
        abstract = True

    code = models.CharField(max_length=32, editable=False, unique=True)

    def save(self, *args, **kwargs):
        self.code = utils.get_unique_code(
            self._is_code_unique,
            self.code,
            length=self.__class__.code.field.max_length
        )
        super(BaseCode, self).save(*args, **kwargs)

    def _is_code_unique(self, code):
        query = self._meta.model.objects.filter(code=code)
        if self.pk:
            query = query.exclude(pk=self.pk)
        return not query.exists()


class Quiz(BaseCode):
    title = models.CharField(
        max_length=255,
        validators=[MinLengthValidator(10)]
    )

    def __str__(self):
        return f"Quiz {self.id}: {self.title}"


class Question(BaseCode):
    question = models.CharField(
        max_length=255,
        validators=[MinLengthValidator(10)]
    )
    quiz = models.ForeignKey(
        Quiz, on_delete=models.CASCADE,
        related_name="questions",
    )

    def clean(self):
        count = self.answer_options.filter(is_correct_answer=True).count()
        if count > 1:
            raise ValidationError("There can only be one correct answer to a question")


class AnswerOption(BaseCode):
    answer = models.CharField(max_length=255)
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE,
        related_name="answer_options"
    )
    is_correct_answer = models.BooleanField(null=True)


class Token(BaseCode):
    # The user that requested the token
    # This should be retrieved in such a way that it's not directly input by the user
    user = models.CharField(max_length=30)
    # TODO: Clean up old tokens
    created = models.DateTimeField(auto_now_add=True)


class Evaluation(models.Model):
    username = models.CharField(
        max_length=30,
        validators=[MinLengthValidator(3)]
    )
    quiz = models.ForeignKey(
        Quiz, on_delete=models.CASCADE,
        related_name="evaluations"
    )
    attempts = models.IntegerField(default=0)
    solved = models.BooleanField()

    class Meta:
        unique_together = (
            "username",
            "quiz"
        )
