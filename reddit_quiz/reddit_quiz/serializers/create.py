"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import typing

from rest_framework.exceptions import ValidationError
from rest_framework.fields import empty
from rest_framework.serializers import ModelSerializer, ListSerializer

from reddit_quiz.models import AnswerOption, Question, Quiz


class AnswerOptionListSerializer(ListSerializer):

    def run_validation(self, data: typing.Union[empty, typing.List] = empty):
        validated_data = super().run_validation(data=data)
        self.validate_correct_answer_count(data)
        return validated_data

    def validate_correct_answer_count(self, validated_data):
        correct_answer_count = len([
            datum for datum in validated_data
            if datum.get("is_correct_answer")
        ])
        if correct_answer_count == 0:
            raise ValidationError(
                detail="There must be at least one correct answer",
                code="no_correct_answer",
            )
        if correct_answer_count != 1:
            raise ValidationError(
                detail="There can only be one correct answer",
                code="correct_answer_not_unique",
            )


class AnswerOptionSerializer(ModelSerializer):
    class Meta:
        model = AnswerOption
        fields = (
            "question",
            "answer",
            "is_correct_answer"
        )
        read_only_fields = (
            "question",
        )
        list_serializer_class = AnswerOptionListSerializer


class QuestionSerializer(ModelSerializer):
    class Meta:
        model = Question
        fields = (
            "quiz",
            "question",
            "answer_options",
        )

        read_only_fields = (
            "quiz",
        )

    answer_options = AnswerOptionSerializer(many=True)

    def create(self, validated_data):
        answer_options_data = validated_data.pop("answer_options")
        self.instance = Question.objects.create(**validated_data)

        answer_option_serializer = self.fields.fields["answer_options"]
        answer_option_serializer.create([
            {
                "question": self.instance,
                **answer_option_data
            }
            for answer_option_data in answer_options_data
        ])

        return self.instance


class QuizSerializer(ModelSerializer):
    class Meta:
        model = Quiz
        fields = (
            "title",
            "questions",
        )

    questions = QuestionSerializer(many=True)

    def create(self, validated_data):
        questions_data = validated_data.pop("questions")
        self.instance = Quiz.objects.create(**validated_data)

        questions_serializers = self.fields.fields["questions"]
        questions_serializers.create([
            {
                "quiz": self.instance,
                **question_data
            }
            for question_data in questions_data
        ])

        return self.instance
