"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer, Serializer

from reddit_quiz.models import Evaluation, Question, AnswerOption, Quiz


class EvaluationSerializer(ModelSerializer):
    class Meta:
        model = Evaluation
        read_only_fields = (
            "solved",
            "attempts",
        )
        fields = read_only_fields + (
            "username",
        )


class AnswerSerializer(Serializer):
    question = serializers.CharField(max_length=Question.code.field.max_length)
    answer = serializers.CharField(max_length=AnswerOption.code.field.max_length)

    def __init__(self, quiz, **kwargs):
        super().__init__(**kwargs)
        self.quiz = quiz

    def validate(self, validated_data: dict):
        question_code = validated_data["question"]
        questions = Question.objects.filter(code=question_code, quiz=self.quiz)
        if len(questions) != 1:
            raise ValidationError("Question is unknown", code="unknown_question")
        question = questions.first()

        answer_code = validated_data["answer"]
        answers = AnswerOption.objects.filter(
            code=answer_code,
            question=question,
            is_correct_answer=True
        )
        # Naughty users might try and send an answer for a different question
        # ... which is still a wrong answer
        if len(answers) != 1:
            raise ValidationError("Answer is incorrect", code="incorrect_answer")
        validated_data.update({
            "question": question,
            "answer": answers.first(),
        })
        return validated_data


class AnswerOptionSerializer(ModelSerializer):
    class Meta:
        model = AnswerOption
        fields = (
            "answer",
            "code",
        )


class QuestionSerializer(ModelSerializer):
    class Meta:
        model = Question
        read_only_fields = fields = (
            "answer_options",
            "code",
            "question",
        )

    answer_options = AnswerOptionSerializer(many=True)


class EvaluationQuizSerializer(ModelSerializer):
    class Meta:
        model = Quiz
        read_only_fields = fields = (
            "title",
            "questions",
        )

    questions = QuestionSerializer(many=True)
