"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from typing import Callable

from django.utils.crypto import get_random_string


def get_unique_code(
        is_unique_func: Callable[[str], bool],
        code: str = None,
        length: int = 12
) -> str:
    """
    Gets a unique string for a field of a model
    """
    if not code:
        code = get_random_string(length=length)

    while not is_unique_func(code):
        code = get_random_string(length=length)

    return code
