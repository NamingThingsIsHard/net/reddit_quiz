let app = new Vue({
  el: "#app",
  // For compatibility with django+jinja which can't escape the final }}
  delimiters: ["[[", "]]"],
  data: {
    quiz: JSON.parse(document.querySelector("#quiz-data").textContent),
    answers: {},
    error: null,
    resultMessage: null,
    attemptsLeft: MAX_ATTEMPTS,
    solved: false,
  },
  computed: {
    /**
     * Creates a body to use when making evaluation requests to the backend
     *
     * @return {{question: String, answer: String}[]}
     */
    requestBody() {
      return Object.keys(this.answers).map((key) => {
        return {
          question: key,
          answer: this.answers[key]
        }
      })
    },
    isValid() {
      return Object.keys(this.answers).length === this.quiz.questions.length
    },
  },
  methods: {
    recordAnswer(question, answerOption) {
      Vue.set(this.answers, question.code, answerOption.code)
    },
    async submit() {
      try {
        // The target URL should be the same URL just with POST params
        let res = await postData(location.toString(), this.requestBody);
        let resJson = await res.json();

        let error = false;
        let resultMessage = "Oh oh, there was an error :("
        // TODO: Replace this with something a bit prettier
        switch (res.status) {
          case 200:
            resultMessage = "You solved it!";
            this.solved = true;
            break
          case 400:
          case 406:
            if (res.status === 400) {
              this.attemptsLeft = 0;
            } else {
              this.attemptsLeft = MAX_ATTEMPTS - resJson.attempts;
            }
            let attemptsLeftString = this.attemptsLeft > 0 ? this.attemptsLeft : "no";
            error = true;
            resultMessage = `Fail! You have ${attemptsLeftString} attempts left.`;
            break
          default:
            error = true;
            resultMessage = resJson.detail || resultMessage;
            console.error(resJson);
        }
        this.error = error;
        this.resultMessage = resultMessage;

      } catch (e) {
        this.error = true;
        this.resultMessage = "Critical error!";
        console.error(e);
      }

    }
  },
});

console.clear();
