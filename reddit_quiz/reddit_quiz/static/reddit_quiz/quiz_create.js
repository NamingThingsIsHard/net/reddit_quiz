let app = new Vue({
  el: "#app",
  // For compatibility with django+jinja which can't escape the final }}
  delimiters: ["[[", "]]"],
  data: {
    form: {
      "title": "",
      "questions": []
    },
    nextQuestionIdCounter: 1,
    nextAnswerIdCounter: 1,
    isValid: true,
    created: false,
  },
  methods: {
    addQuestion() {
      this.form.questions.push({
        id: this.nextQuestionIdCounter++,
        question: "",
        answer_options: []
      });
    },
    addAnswerOption(question) {
      question.answer_options.push({
        id: this.nextAnswerIdCounter++,
        answer: "",
        is_correct_answer: question.answer_options.length === 0
      });
    },
    updateSelectedAnswer(answerOptions, selectedOption) {
      for (let answerOption of answerOptions) {
        //this.$set(answerOption, "is_correct_answer", answerOption.id === selectedOption.id)
        answerOption.is_correct_answer = answerOption.id === selectedOption.id
      }
    },
    delFromSequence(object, sequence) {
      let i = sequence.findIndex((o) => o.id === object.id);
      if (i !== null) {
        sequence.splice(i, 1);
      }
    },
    async submit() {
      try {
        // The target URL should be the same URL just with POST params
        let response = await postData(location.toString(), this.form);
        const handler = this[`_handle_${response.status}`];
        if (handler instanceof Function) {
          await handler(response);
        }
      } catch (e) {
        console.error(e)
      }
    },
    async _handle_200(response) {
      this.created = true;
    },
    async _handle_201(response) {
      return this._handle_200(response)
    },

  },
});

console.clear();
