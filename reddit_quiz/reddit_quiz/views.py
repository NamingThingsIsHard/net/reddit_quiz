"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf import settings
from django.shortcuts import render
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response

from reddit_quiz.models import Quiz, Evaluation
from reddit_quiz.permissions import HasToken
from reddit_quiz.serializers.create import QuizSerializer
from reddit_quiz.serializers.evaluation import EvaluationSerializer, AnswerSerializer, \
    EvaluationQuizSerializer


class GenericTokenView(GenericAPIView):
    permission_classes = [HasToken]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.token = None


class CreateQuizView(CreateModelMixin, GenericTokenView):
    serializer_class = QuizSerializer

    def get(self, request, *args, **kwargs):
        return render(request, "reddit_quiz/quiz_create.html", context={
            "DEV": settings.DEBUG
        })

    def post(self, request, *args, **kwargs):
        response = self.create(request, *args, **kwargs)
        # Remove token to make sure no more quizes can be created with it
        self.token.delete()
        return response


class QuizError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_code = "quiz"


class QuizEvaluationView(GenericTokenView):
    # One can only evaluate a quiz
    queryset = Quiz.objects.all()
    lookup_field = "code"
    lookup_url_kwarg = "code"

    def get(self, request, *args, **kwargs):
        serializer = EvaluationQuizSerializer()
        return render(request, "reddit_quiz/quiz_evaluate.html", context={
            "DEV": settings.DEBUG,
            "MAX_ATTEMPTS": settings.RQ_MAX_ATTEMPTS,
            "quiz": serializer.to_representation(self.get_object()),
        })

    def post(self, *args, **kwargs):
        quiz = self.get_object()

        # Eval object creation
        try:
            evaluation = self._make_evaluation(quiz)
        except QuizError:
            self.token.delete()
            raise
        evaluation.full_clean()
        evaluation.save()

        # Tokens of solved quizzes shouldn't be kept
        data = EvaluationSerializer(instance=evaluation).data
        if evaluation.solved:
            self.token.delete()
            response_status = status.HTTP_200_OK
        else:
            if evaluation.attempts >= settings.RQ_MAX_ATTEMPTS:
                self.token.delete()
            response_status = status.HTTP_406_NOT_ACCEPTABLE

        return Response(data=data, status=response_status)

    def _make_evaluation(self, quiz):
        eval_dict = {
            "quiz": quiz,
            "username": self.token.user
        }

        # Not get_or_create because we don't want to write to the DB until the end of the process
        evaluation = Evaluation.objects.filter(**eval_dict).first()
        if not evaluation:
            evaluation = Evaluation(**eval_dict)
        if evaluation.attempts >= settings.RQ_MAX_ATTEMPTS:
            raise QuizError("Passed max attempts to solve")
        if evaluation.solved:
            raise QuizError("Quiz already solved")

        # Make sure responses were provided for all questions
        answer_serializer = AnswerSerializer(
            quiz,
            many=True,
            data=self.request.data
        )
        evaluation.solved = answer_serializer.is_valid()
        evaluation.attempts += 1

        return evaluation
