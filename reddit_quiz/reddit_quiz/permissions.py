"""
reddit-quiz
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from rest_framework.permissions import BasePermission
from rest_framework.request import Request

from reddit_quiz.models import Token
from reddit_quiz.serializers.permissions import TokenParamSerializer


class HasToken(BasePermission):
    """
    Checks for ?token=<token> in URL of request

    Views will then have a token field available to them
    """

    def has_permission(
            self,
            request: Request,
            view: 'reddit_quiz.views.GenericTokenView'
    ):
        serializer = TokenParamSerializer(data=request.query_params)
        has_permission = False
        if serializer.is_valid():
            try:
                view.token = Token.objects.get(code=serializer.validated_data["token"])
                has_permission = True
            except Token.DoesNotExist:
                pass
        return has_permission
