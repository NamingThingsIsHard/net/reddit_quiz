.. image:: https://readthedocs.org/projects/reddit-quiz/badge/?version=latest
    :target: https://reddit-quiz.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

The Reddit Quiz is a system that requires the user pass a quiz about the post
before they are able to comment.
It emulates the Wordpress plugin Know2Comment_ by NRKbeta_

The two major components are the website that handles quiz creation and evaluation,
and the reddit bot which takes care of moderating a subreddit.
The reddit bot can be bound to a subreddit by giving it mod rights.

Whether you're a dev, future admin or mod, you should read the docs_ 🙂

Features
--------

**Moderation**

Activating it will check comments on posts to see if the user completed the quiz successfully.
Posts that don't have a quiz will also be deleted after a grace period.

**Quiz creation**

When users create posts they will be able to associate quiz to them using the website.
Quiz necessity can be tied to different types of posts e.g only external links.

**Quiz evaluation**

Each post will have a top comment with a link to the quiz that people have to take in order to be allowed comment.


.. _docs: https://reddit-quiz.readthedocs.io/en/latest/
.. _Know2Comment: https://github.com/nrkbeta/nrkbetaquiz
.. _NRKbeta: https://nrkbeta.no/2017/08/10/with-a-quiz-to-comment-readers-test-their-article-comprehension/
