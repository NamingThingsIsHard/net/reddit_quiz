=================
Sequence diagrams
=================


Quiz creation
-------------

.. mscgenjs::
    :language: msgenny

    wordwraparcs=true, width=auto;

    User => Reddit : "Make post";
    Reddit -> RQ : "User made post";
    RQ -> RQ : "Set timer for\nquiz creation";
    RQ => Reddit : "Private mail with link to quiz creation";
    User => Reddit : "Check mail";
    User => RQ : "/quiz/create/{token_id}";
    RQ -> RQ : "Store\nquiz options";
    RQ => User : "Thank you + Quiz link";


.. _sd-commenting:

Commenting
----------

.. mscgenjs::
    :language: msgenny

    wordwraparcs=true, width=auto;


    User => Reddit : "Open post";
    Reddit -> User: ;
    User => RQ : PM bot with post link;
    RQ -> User : Quiz link with token;
    User => RQ : "Open quiz";
    RQ -> User: Quiz ;
    User => RQ: "Quiz answers";
    RQ -> User: "You may comment";

    User => Reddit: Comment;

.. _sd-evaluation:

Evaluation
----------

Each new comment in the subreddit has to be evaluated to make sure it's permitted.
When everything's OK, nothing is done.

.. note::

    The check can be configured to check all comments or only top-level comments.
    The former is recommended in order to attempt the promotion of more informed and on-topic discussions.

.. mscgenjs::
    :language: msgenny

    wordwraparcs=true, width=auto;

    RQ -> RQ : "Verify quiz result";
    Reddit alt RQ : "Failed quiz" {
      RQ -> RQ : "Increase fail counter for post";
      RQ -> Reddit : "Delete user comment";
      Reddit alt RQ : "Below max fails" {
        RQ -> Reddit : "Notify user to take quiz";
        --- : "Failed too many times";
        RQ -> Reddit : "Notify user that max fails reached";
      };
    };
