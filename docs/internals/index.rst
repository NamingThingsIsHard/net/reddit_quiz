Internals
=========

.. toctree::

    sequence_diagrams

This might be interesting for people who would like to know what's going on
behind the scenes in order to understand the process more deeply.

Hopefully, it's not arcane and makes sense.
The idea isn't very complicated.
