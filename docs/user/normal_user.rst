User guide
==========

As a user, you will be directly impacted by the bot.
There are two use cases

Commenting
----------

In order to comment, you will need to pass a quiz about the relevant post!
Quizzes are made by the poster.
For now, this process involves a few steps, which will be dwindled down in v2.

.. note::
    Future versions might add moderation options to ban users based on the quality of their quizzes.

1. Send a message to the bot with the link
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a necessary step as simply providing a link to a quiz would require a user to input their
username, which is ripe for abuse.
You could enter somebody else's username for example, fail the quiz often enough and block another
user from commenting.


2. Solve the quiz
^^^^^^^^^^^^^^^^^

The bot will respond with a personal link that allows the user to attempt solving the quiz.
There is a maximum number of attempts which in the future will depend on the subreddit configuration.

3. Comment!
^^^^^^^^^^^

With the quiz solved, users can now finally comment on a post.

Posting
-------

Since users require a quiz in order to comment, post creators will have to create an accompanying quiz.
There will be a timer on quiz creation during which the thread will be locked.
Commenting will not be possible during this time.

There is only one step

**Create a quiz using link received by PM from the bot**

The link is personal and links the creator the post.
There are only a few restrictions placed on the quiz:

#. At least 3 questions
#. At least 2 choices per question
#. A minimum character count on the questions and choices
   Questions at least 10
   Answers at least 2
#. No multiple choice

Once the quiz is created, the thread will be unlocked and comments allowed.
