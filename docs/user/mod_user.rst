Documentation for mods
======================

As a moderator of a subreddit who wishes to activate the bot the procedure is easy:
simply give the bot mod powers.
The bot will receive a message from reddit notifying it of its mod status.
It will of course check if it really has mod powers.

That's it. Configuration options will come in another version.
Check the roadmap for this and similar planned features.
