.. Reddit Quiz documentation master file, created by
   sphinx-quickstart on Sun Aug 30 21:31:34 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Reddit Quiz's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Documentation tree:

   user/index
   internals/index

.. include:: ../README.rst
